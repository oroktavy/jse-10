package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.ICommandRepository;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT,
            ArgumentConst.ARG_ABOUT,
            "Show general application info"
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION,
            ArgumentConst.ARG_VERSION,
            "Show application version"
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP,
            ArgumentConst.ARG_HELP,
            "Show possible options"
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO,
            ArgumentConst.ARG_INFO,
            "Show system information"
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT,
            null,
            "Close application"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS,
            null,
            "Show application arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS,
            null,
            "Show application commands"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST,
            null,
            "Show all tasks"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE,
            null,
            "Create a task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR,
            null,
            "Clear task list"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST,
            null,
            "Show all projects"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE,
            null,
            "Create a project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR,
            null,
            "Clear project list"
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, HELP, INFO, ARGUMENTS, COMMANDS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
