package ru.aushakov.tm.model;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    public Command() {
    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (this.name != null && !this.name.isEmpty()) result += this.name;
        if (this.arg != null && !this.arg.isEmpty()) result += ": [" + this.arg + "]";
        if (this.description != null && !this.description.isEmpty()) result += " - " + this.description;
        return result;
    }
}
