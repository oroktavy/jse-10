package ru.aushakov.tm.api;

public interface ICommandController {

    void showVersion();

    void showAppInfo();

    void showHelp();

    void showSystemInfo();

    void showArguments();

    void showCommands();

    void exit();

}
