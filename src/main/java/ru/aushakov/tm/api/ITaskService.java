package ru.aushakov.tm.api;

import ru.aushakov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    void add(Task task);

    Task add(String name, String description);

    void remove(Task task);

    void clear();

}
