package ru.aushakov.tm.api;

import ru.aushakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
