package ru.aushakov.tm.api;

import ru.aushakov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    void add(Project project);

    Project add(String name, String description);

    void remove(Project project);

    void clear();

}
