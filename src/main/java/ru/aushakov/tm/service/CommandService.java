package ru.aushakov.tm.service;

import ru.aushakov.tm.api.ICommandService;
import ru.aushakov.tm.model.Command;
import ru.aushakov.tm.api.ICommandRepository;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
