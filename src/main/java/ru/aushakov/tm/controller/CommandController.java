package ru.aushakov.tm.controller;

import ru.aushakov.tm.api.ICommandController;
import ru.aushakov.tm.api.ICommandService;
import ru.aushakov.tm.model.Command;
import ru.aushakov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showAppInfo() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: ANDREY USHAKOV");
        System.out.println("E-MAIL: oroktavy@gmail.com");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    @Override
    public void showSystemInfo() {
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("[SYSTEM INFO]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory (bytes): " + NumberUtil.bytesToText(freeMemory));
        System.out.println("Maximum memory (bytes): " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : NumberUtil.bytesToText(maxMemory)));
        System.out.println("Total memory available to JVM (bytes): " +
                NumberUtil.bytesToText(totalMemory));
        System.out.println("Used memory by JVM (bytes): " + NumberUtil.bytesToText(usedMemory));
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENT LIST]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String argument = command.getArg();
            if (argument == null) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMAND LIST]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
